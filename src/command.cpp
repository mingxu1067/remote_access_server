#include "command.h"
#include "unit.h"

#include <unistd.h>
#include <sstream>
#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <errno.h>

std::map< std::string, int> Command::command_type_map = {
    {"exit", INTER_COMMAND_TYPE},
    {"setenv", INTER_COMMAND_TYPE},
    {"printenv", INTER_COMMAND_TYPE},
    {"ls", EXTER_COMMAND_TYPE},
    {"cat", EXTER_COMMAND_TYPE},
    {"removetag", EXTER_COMMAND_TYPE},
    {"removetag0", EXTER_COMMAND_TYPE},
    {"number", EXTER_COMMAND_TYPE},
    {"noop", EXTER_COMMAND_TYPE}
};

std::map< std::string, Command::handler > Command::inter_command_handler_map = {
    {"exit", Command::exitShell},
    {"setenv", Command::setEnv},
    {"printenv", Command::printEnv}
};

// ============================== Public ==============================
Command::Command(std::string name, std::string arguments) {
    this->name = unit::deleteStringFrontSpace(name);
    parseArguments(arguments);
    this->filename = std::string("");
    this->pipe_number = 0;
}

Command::~Command() {

}

std::string Command::getName() {
    return name;
}

std::vector< std::string > Command::getArguments() {
    return arguments;
}

void Command::setArgumnets(std::string arguments) {
    this->arguments.clear();
    parseArguments(arguments);
}

int Command::getPipeNumber() {
    return pipe_number;
}

void Command::setPipeNumber(int pipe_number) {
    this->pipe_number = pipe_number;
}

std::string Command::getFilename() {
    return filename;
}

void Command::setFilename(std::string filename) {
    this->filename = filename;
}

void Command::exec() {
    int type = getCommandType();

    if (type == UNKNOW_COMMAND_TYPE) {
        fprintf(stderr, "Unknown command: [%s].\n", name.c_str());
    } else if (type == INTER_COMMAND_TYPE) {
        (*inter_command_handler_map.find(name)->second)(arguments);

    } else {
        const char *argv[arguments.size() + 2];
        argv[0] = name.c_str();

        for (int i=0; i<arguments.size(); i++) {
            argv[i+1] = arguments[i].c_str();
        }

        argv[arguments.size() + 1] = NULL;

        execvp(argv[0], (char **) argv);
        fprintf(stderr, "Unknown command: [%s].\n", name.c_str());
        exit(1);
    }
}

int Command::getCommandType() {
    std::map< std::string, int>::iterator it = command_type_map.find(name);
    if (it != command_type_map.end()) {
        return it->second;
    }

    return UNKNOW_COMMAND_TYPE;
}

// ============================== Private ==============================
void Command::exitShell(std::vector<std::string> arguments) {
    exit(0);
}

void Command::setEnv(std::vector<std::string> arguments) {
    if (arguments.size() < 2) {
        std::cout << "The argument amount of setenv should exactly euqal 2." << std::endl;
    }
    setenv(arguments[0].c_str(), arguments[1].c_str(), 1);
}

void Command::printEnv(std::vector<std::string> arguments) {
    for (std::string arg : arguments) {
        const char *result = getenv(arg.c_str());
        if (result) {
            std::cout << arg << "=" << result << std::endl;
        }
    }
}

void Command::parseArguments(std::string argument_str) {
    argument_str = unit::deleteStringFrontSpace(argument_str);

    if (argument_str.size() > 1) {

        std::istringstream iss(argument_str);
        while (!iss.eof()) {
            std::string arg;
            iss >> arg;
            if (arg.compare("\0") != 0)
                this->arguments.push_back(arg);
        }
    }
}