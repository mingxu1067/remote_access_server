#include "shell.h"
#include "unit.h"

#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/wait.h>

// ============================== Public ==============================
Shell::Shell() {}

Shell::~Shell() {}

void Shell::run() {
    showWelcomeMessage();

    while (true) {
        prompt();
        std::vector<Command> commands = parseCommand(read_command());
        execCommand(commands);
    }
}

// ============================== Private ==============================
void Shell::showWelcomeMessage() {
    std::cout
            << "****************************************\n"
            << "** Welcome to the information server. **\n"
            << "****************************************\n";
}

void Shell::prompt() {
    std::cout << "% ";
}

std::string Shell::read_command() {
    std::string command("");

    do {
        std::string temp;
        getline(std::cin, temp);

        command.append(temp);
    } while((command.at(command.size()-1) != '\n') && 
            (command.at(command.size()-1) == '\r') && 
            (command.at(command.size()-1) == '\0'));

    command[command.size()-1] = '\0';

    return command;
}

std::vector<Command> Shell::parseCommand(std::string input) {
    std::vector<Command> commands;

    std::string command_name("");
    std::string arguments("");

    bool has_read_command = false;
    std::string *temp = &command_name;

    for (int i=0; i<input.size(); i++) {
        char c = input[i];

        if ((i == input.size()-1) && command_name.size()) {
            Command cmd(command_name, arguments);
            commands.push_back(cmd);
            break;
        }

        if (c == PIPE_SYMBOL) {
            has_read_command = false;
            temp = &command_name;

            Command cmd(command_name, arguments);
            command_name.clear();
            arguments.clear();

            std::string next_word("");
            for (i=i+1;i<input.size()-1;i++) {
                c = input[i];
                if (c == ' ') {
                    break;
                } else {
                    next_word.append(1u, c);
                }
            }

            int pipe_number = 1;
            if (next_word.size()) {
                if (unit::isDigits(next_word)) {
                    pipe_number = atoi(next_word.c_str());
                } else {
                    command_name = next_word;
                    has_read_command = true;
                    temp = &arguments;
                }
            }

            cmd.setPipeNumber(pipe_number);

            if (cmd.getName().size()) {
                commands.push_back(cmd);
            }

        } else if (c == FILE_REDIRECTION_SYMBOL) {
            std::string filename("");
            for (i=i+1; i<input.size()-1;i++) {
                c = input[i];
                if (c == ' ') {
                    if (filename.size()) {
                        break;
                    }
                } else if (c == PIPE_SYMBOL) {
                    i -= 1;
                    break;
                } else {
                    filename.append(1u, c);
                }
            }

            Command cmd(command_name, arguments);
            cmd.setPipeNumber(0);
            cmd.setFilename(filename);

            command_name.clear();
            arguments.clear();

            commands.push_back(cmd);

        } else if (c == ' ') {
            if (has_read_command) {
                temp = &arguments;
                temp->append(1u, c);
            } else {
                temp = &command_name;
            }

        } else {
            has_read_command = true;
            temp->append(1u, c);

        }
    }

    return commands;
}

void Shell::execCommand(std::vector<Command> commands) {
    for (int cmd_index=0; cmd_index < commands.size(); cmd_index++) {
        Command cmd = commands[cmd_index];
        passPipe();

        int cmd_type = cmd.getCommandType();

        int fd_stdin = STDIN_FILENO;
        int fd_stdout = STDOUT_FILENO;

        int fd[2];

        std::vector<int> child_close_list;
        std::vector<int> parent_close_list;

        for (Pipe p : pending_pipes) {
            if (p.getCount() == 0) {
                fd_stdin = p.getFdRead();
            } else {
                child_close_list.push_back(p.getFdRead());
            }
        }

        if (cmd.getPipeNumber()) {
            Pipe *temp_pipe = getPipe(cmd.getPipeNumber());
            if (temp_pipe != nullptr) {
                fd_stdout = temp_pipe->getFdWrite();
            } else {
                if (pipe(fd) == -1) {
                    std::cout << "Fail to create pipe." << std::endl;
                }

                Pipe out_pipe = Pipe(fd[0], fd[1], cmd.getPipeNumber());
                pending_pipes.push_back(out_pipe);
                child_close_list.push_back(fd[0]);
                fd_stdout = fd[1];
            }
        }

        if (cmd.getFilename().size()) {
            fd_stdout = open(cmd.getFilename().c_str(), O_WRONLY | O_CREAT | O_TRUNC | O_CLOEXEC, 000777);
        }

        if (cmd_type == UNKNOW_COMMAND_TYPE) {
            cmd.exec();
            if (cmd_index != 0)
                backPipe();

            if (getPipe(0) != nullptr) {
                close(getPipe(0)->getFdWrite());
            }

            break;
        } else if (cmd_type == INTER_COMMAND_TYPE) {
            if (getPipe(0) != nullptr) {
                close(getPipe(0)->getFdWrite());
            }

            cmd.exec();
        } else {

            if (getPipe(0) != nullptr) {
                close(getPipe(0)->getFdWrite());
            }


            pid_t pid = fork();
            if (pid < 0) {
                std::cout << "Fail to fork." << std::endl;
                exit(1);
            } else if (pid == 0) {
                dup2(fd_stdin, STDIN_FILENO);
                dup2(fd_stdout, STDOUT_FILENO);
                if (fd_stdin != STDIN_FILENO) {
                    close(fd_stdin);
                }

                for (int close_fd : child_close_list) {
                    close(close_fd);
                }

                cmd.exec();

            } else {
                if (getPipe(0) != nullptr) {
                    close(getPipe(0)->getFdRead());
                }

                int status_code = 0;
                waitpid(pid, &status_code, 0);
            }
        }

        cleanPipe();
    }

}

Pipe* Shell::getPipe(int count) {
    for (int i=0; i<pending_pipes.size(); i++) {
        if (pending_pipes[i].getCount() == count) {
            return &pending_pipes[i];
        }
    }

    return nullptr;
}

void Shell::passPipe() {
    for (int i=0; i<pending_pipes.size(); i++) {
        pending_pipes[i].passCount();
    }
}

void Shell::backPipe() {
    for (int i=0; i<pending_pipes.size(); i++) {
        pending_pipes[i].addCount();
    }
}

void Shell::cleanPipe() {
    for (int i=0; i<pending_pipes.size(); i++) {
        if (pending_pipes[i].getCount() <= 0) {
            pending_pipes.erase(pending_pipes.begin()+i);
        }
    }
}
