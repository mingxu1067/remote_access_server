#include "shell.h"

#include <cstdlib>
#include <cstring>
#include <string>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include<signal.h>


void zombie_handler (int signal_code) {
    int status_code;
    wait(&status_code);
}

int main(int argc, char const *argv[])
{
    int port = 3391;
    std::string shell_directory(".");

    for (int i = 1; i < argc; i++) {
        std::string para(argv[i]);
        if ((para.find("--port=") == 0) || (para.find("-p=") == 0)) {
            port = atoi(para.substr(para.find("=") + 1).c_str());
        } else if ((para.find("--shell_directory=") == 0) || (para.find("-sd=") == 0)) {
            shell_directory = para.substr(para.find("=") + 1);
        }
    }

    if (chdir(shell_directory.c_str()) == -1) {
        printf("The directory \"%s\" does not exist.\n", shell_directory.c_str());
        exit(1);
    }

    int listen_fd;
    struct sockaddr_in listen_address;
    memset((char*)&listen_address, 0, sizeof(listen_address));
    
    listen_address.sin_family = AF_INET;
    listen_address.sin_addr.s_addr = htonl(INADDR_ANY);
    listen_address.sin_port = htons(port);

    if ((listen_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        fprintf(stderr, "Fail to create socket.\n");
    }

    if(bind(listen_fd, (struct sockaddr *) &listen_address, sizeof(listen_address)) == -1) {
        fprintf(stderr, "Fail to bind socket.\n");
        exit(1);
    }

    if(listen(listen_fd, 0) == -1) {
        fprintf(stderr, "Fail to listen socket.\n");
        exit(1);
    }

    signal(SIGCHLD, zombie_handler);

    while (true) {
        int client_fd = -1;

        if (client_fd = accept(listen_fd, NULL, NULL)) {

            pid_t pid = fork();
            if (pid == 0) {
                printf("Create shell to serve the client %d\n", getpid());

                dup2(client_fd, STDIN_FILENO);
                dup2(client_fd, STDOUT_FILENO);
                dup2(client_fd, STDERR_FILENO);

                close(client_fd);
                close(listen_fd);

                // call shell to serve.
                setenv("PATH","bin:.",1);
                Shell shell = Shell();
                shell.run();

                exit(0);

            } else {
                close(client_fd);
            }
        }
    }

    close(listen_fd);
    return 0;
}