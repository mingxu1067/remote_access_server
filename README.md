# Remote Access Server #

The project of the course (Network programming) - Remote Access Server.

## Instruction ##

* `make build` for building
* `make run` for running server.
	* The default work directory and port is `.` and 3391.

## Server Shell ##
* setenv env_name value
	* ex: `setenv PATH bin`
* printenv env_name
	* ex: `printenv PATH`
* exit
	* Close connection
* Other command will be executed, if they are under the $(PATH).
