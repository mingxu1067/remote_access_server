#ifndef RAS_SHELL_H_
#define RAS_SHELL_H_

#include "pipe.h"
#include "command.h"

#include <string>
#include <vector>

#define MAX_LINE_CHAR_NUMBER 10000

#define PIPE_SYMBOL '|'
#define FILE_REDIRECTION_SYMBOL '>'

class Shell {
public:
    Shell();
    ~Shell();

    void run();
private:
    void showWelcomeMessage();
    void prompt();

    std::vector<Command> parseCommand(std::string input);
    void execCommand(std::vector<Command> commands);
    Pipe* getPipe(int count);
    void passPipe();
    void backPipe();
    void cleanPipe();

    std::string read_command();
    std::vector<Pipe> pending_pipes;
};

#endif