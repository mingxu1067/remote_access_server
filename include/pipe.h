#ifndef RAS_PIPE_H_
#define RAS_PIPE_H_

class Pipe {
public:
    Pipe(int fd_read, int fd_write, int count);
    ~Pipe();

    int getFdRead();
    int getFdWrite();
    int getCount();

    void passCount();
    void addCount();

private:
    int fd_read;
    int fd_write;
    int count;
};

#endif