#ifndef RAS_UNIT_H_
#define RAS_UNIT_H_

#include <string>

namespace unit {
    bool isDigits(const std::string &str);
    std::string deleteStringFrontSpace(std::string str);
}

#endif
