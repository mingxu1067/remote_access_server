#ifndef RAS_COMMAND_H_
#define RAS_COMMAND_H_

#include <string>
#include <vector>
#include <map>

#define UNKNOW_COMMAND_TYPE -1
#define INTER_COMMAND_TYPE 0
#define EXTER_COMMAND_TYPE 1

class Command {
public:
    Command(std::string name, std::string arguments);
    ~Command();

    std::string getName();

    std::vector< std::string > getArguments();
    void setArgumnets(std::string arguments);

    int getPipeNumber();
    void setPipeNumber(int pipe_number);
    
    std::string getFilename();
    void setFilename(std::string filename);

    int getCommandType();

    void exec();

private:
    typedef void (*handler)(std::vector<std::string>);
    static std::map< std::string, int> command_type_map;
    static std::map< std::string, handler> inter_command_handler_map;

    static void exitShell(std::vector<std::string> arguments);
    static void setEnv(std::vector<std::string> arguments);
    static void printEnv(std::vector<std::string> arguments);

    std::string name;
    std::vector< std::string > arguments;
    std::string filename;

    int pipe_number;

    void parseArguments(std::string arguments);

};
#endif