CC = g++
CFLAG = -std=c++11

EXEC = ras.exe

INC = -I ./include
SRCS=$(wildcard src/*.cpp)

BINDIR = bin


build:
	@mkdir -p $(BINDIR)
	$(CC) -O3 -o $(BINDIR)/$(EXEC) $(SRCS) $(INC) $(CFLAG)

run: $(BINDIR)/$(EXEC)
	./$(BINDIR)/$(EXEC)

clean:
	@rm $(BINDIR)/$(EXEC)